var express = require('express');
var path = require('path');
var serveStatic = require('serve-static');
var cfenv = require('cfenv');
appEnv = cfenv.getAppEnv();
app = express();

app.use(express.static(__dirname + "/dist"));

// app.use(serveStatic(__dirname + "/dist"));


app.listen(appEnv.port, appEnv.bind, function() {
    console.log("server starting on " + appEnv.url)
  });
  
// var port = process.env.PORT || 8080;
// app.listen(port);
// console.log('server started '+ port);