import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Chart from '@/components/Chart'
import AxisLoad from '@/components/AxisLoad'
import Login from '@/components/Login'
import Register from '@/components/Register'
import LogOut from '@/components/LogOut'
import DataLoad from '@/components/DataLoadingPage'
import store from '../store.js'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/chart',
      name: 'Chart',
      component: Chart
    },
    {
      path: '/axisLoad',
      name: 'AxisLoad',
      component: AxisLoad
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/logOut',
      name: 'LogOut',
      component: LogOut
    },
    {
      path: '/dataLoad',
      name: 'DataLoadingPage',
      component: DataLoad
    }
  ],
  mode: 'history'
})
