import VuetableFieldHandle from 'vuetable-2/src/components/VuetableFieldHandle.vue'
/* eslint-disable */
export default [
  // {
  //   name: VuetableFieldHandle
  // },
  {
    name: 'AbnormalOutput',
    title: '<span class="orange glyphicon glyphicon-user"></span> AbnormalOutput',
    sortField: 'AbnormalOutput'
  },
   'CountingArrival',
  {
    name: 'CurrentValueCounter',
    sortField: 'CurrentValueCounter'
  },
  {
    name: 'R1axisLoad',
    sortField: 'R1axisLoad'
  },
  {
    name:  'SetpointCounter',
    sortField:   'SetpointCounter'
  },
  {
    name:  'XaxisLoad',
    sortField:   'XaxisLoad'
  },
  {
    name:  'YaxisLoad',
    sortField:   'YaxisLoad'
  },
  {
    name:  'ZaxisLoad',
    sortField:   'ZaxisLoad'
  },
  {
    name: '_time',
    title: '<span class="orange glyphicon glyphicon-user"></span> Time',
    sortField: '_time'
  },
//   {
//     name: 'gender',
//     formatter: (value) => {
//       return value === 'M' ? 'Male' : 'Female'
//     }
//   },
  'actions'
]