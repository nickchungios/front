# Getting Started
---
### 1.  I build the app and API on the Heroku and you can just use domain to run the web app.
### 2.  I use vue.js to create my fornt-end and it is a hand made app.
- __[api](https://semens-practice.herokuapp.com/api/v1/)__ 
- __[Web App](https://semens-practice-app.herokuapp.com/)__ 

Node.js
o Via node
package manager - Run
on the project root
npm
npm install

.

# Run the application
---
1. Open your terminal and install vue-cli `npm install -g @vue/cli`.

2. Install packages to the node_module folder.

	`cd seimens-app` and run `npm i` or `sudo npm i `
	
	( Maybe you need to use `-f` to force the npm intall node-sass, likes `npm i -f node-sass` )

3. Run the command `npm run start` or `npm run ` , then you can start the app.


# Result
---
![Minion](https://octodex.github.com/images/minion.png)

###Login Page
![Login](./Img/login.png)
####If you did not log in
![noLogin](./Img/noLogin.png)
###SignUp Page
![SignUp](./Img/signup.png)
###Table Page
####Data is loading
![Loading](./Img/loading.png)
####Data is loaded
![Table](./Img/table.png)
![Table2](./Img/table2.png)
###Chart
####Counter
![Counter](./Img/counterChart.png)
####AxisLoad
![AxisLoad](./Img/axisLoad.png)
#####R1
![R1](./Img/R1.png)
#####X
![X](./Img/X.png)
#####Y
![Y](./Img/Y.png)
#####Z
![Z](./Img/Z.png)








